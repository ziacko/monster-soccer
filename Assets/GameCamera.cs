﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour
{

	//keep a copy of the camera's rotation and FOV
	// Use this for initialization
	Vector3 cameraRotation;
	const float defaultFOV = 60.0f;
	const float minFOV = 1.0f;
	const float zoomSpeed = 25.0f;
	public UnityEngine.Camera gameCamera;
	//public bool trackBall = false;

	void Start()
	{
		
		gameCamera = GetComponent<UnityEngine.Camera>();
		cameraRotation = gameCamera.transform.eulerAngles;
		//gameObject.transform.rotation

	}

	// Update is called once per frame
	void Update()
	{

	}

	public void Reset()
	{
		if (gameCamera != null)
		{
			transform.rotation = Quaternion.Euler(cameraRotation);
			gameCamera.fieldOfView = defaultFOV;
		}
	}

	public void TrackBall(GameObject ball)
	{
		transform.LookAt(ball.transform.position);
		if (gameCamera.fieldOfView > minFOV)
		{
			gameCamera.fieldOfView -= (zoomSpeed * Time.deltaTime);
		}
		//look at and slowly zoom into the ball
	}
}
