﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;
public class HUD : MonoBehaviour {
	
	//public GameObject HUDPrefab;
	//public Camera menuCamera;
	public float canvasDistance = 10.0f;
	public worldhandler handler = null;
	//public GameObject

	//get both text boxes
	public Canvas menuCanvas;
	public Text team1Score;
	public Image team1Image;
	public Text team2Score;
	public Image team2Image;
	public Text flavorText;
	//public RawImage splashImage;

	public Image scoreLeftBackground;
	public Image scoreRightBackground;
	public Image[] scoreLeft;
	public Image[] scoreRight;

	public RawImage goalImage;

	public GameObject winningTeam;

	//this holds both scoreboards.
	GameObject scoreBoard;

	// Use this for initialization
	void Start () {
		menuCanvas = GetComponent<Canvas>();
		team1Score = GameObject.Find("Score Board/Board Left/ScoreImage/Team").GetComponent<Text>();
		team1Image = GameObject.Find("Score Board/Board Left/ScoreImage").GetComponent<Image>();
		team2Score = GameObject.Find("Score Board/Board Right/ScoreImage/Team").GetComponent<Text>();
		team2Image = GameObject.Find("Score Board/Board Right/ScoreImage").GetComponent<Image>();
		winningTeam = GameObject.Find("Win Team");
		winningTeam.SetActive(false);
		//flavorText = GameObject.Find("Flavor Text").GetComponent<Text>();
		//splashImage = GameObject.Find("Splash Image").GetComponent<RawImage>();
		//splashImage.enabled = false;
		scoreBoard = GameObject.Find("Score Board");
		goalImage = GameObject.Find("Goal Image").GetComponent<RawImage>();
		goalImage.enabled = false;
		//flavorText.text = "first team to " + winningValue.ToString() + " goals wins!";
		//team1Score.color = new Color(0.45f, 0.45f, 1.0f);
		//menuCanvas.transform.position = new Vector3(0.0f, 0.0f, -300.0f);
		scoreLeftBackground = GameObject.Find("Score Board/Board Left/ScoreBackground/").GetComponent<Image>();
		scoreLeft = GameObject.Find("Score Board/Board Left/ScoreBackground/").GetComponentsInChildren<Image>();
		System.Array.Reverse(scoreLeft);
		scoreRightBackground = GameObject.Find("Score Board/Board Right/ScoreBackground/").GetComponent<Image>();
		scoreRight = GameObject.Find("Score Board/Board Right/ScoreBackground/").GetComponentsInChildren<Image>();
		System.Array.Reverse(scoreRight);
		
		//winningTeam.gameObject.SetActive(false);
		DisableHUD(false);
		//scoreBoard.SetActive(false);

		for (uint scoreIter = 0; scoreIter < 5; scoreIter++)
		{
			scoreLeft[scoreIter].color = Color.black;
			scoreRight[scoreIter].color = Color.black;
		}

		//scoreBoard.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void ShowWinner(worldhandler.teamName_t name, bool show = false)
	{
		winningTeam.SetActive(show);
		winningTeam.GetComponentInChildren<Text>().text = (name == worldhandler.teamName_t.australia) ? "Australia has won! Congratulations" : "England has won! Congratulations";
	}

	public void ShowSuddenDeath(string text)
	{
		winningTeam.SetActive(true);
		winningTeam.GetComponentInChildren<Text>().text = text;
	}

	public void reset()
	{
		//splashImage.enabled = false;
		//team1Score.color = new Color(0.45f, 0.45f, 1.0f);
		HideScores(true);
		DisableHUD(false);
		//scoreBoard.SetActive(false);
		winningTeam.SetActive(false);
		for (uint scoreIter = 0; scoreIter < 5; scoreIter++)
		{
			scoreLeft[scoreIter].color = Color.black;
			scoreRight[scoreIter].color = Color.black;
		}

		scoreBoard.SetActive(false);
	}

	public void HideScores(bool hide)
	{
		scoreLeftBackground.enabled = hide;
		scoreRightBackground.enabled = hide;

		for(uint iter = 0; iter < 5; iter++)
		{
			scoreLeft[iter].enabled = hide;
			scoreRight[iter].enabled = hide;
		}
	}

	public void DisableHUD(bool enable)
	{
		scoreBoard.SetActive(enable);
	}

	public void Disable(bool disable)
	{
		DisableHUD(false);
		//lavorText.enabled = false;
		//splashImage.enabled = false;
	}

	public void UpdateBoard(worldhandler.team currentTeam, worldhandler.teamName_t name)
	{
		for (uint scoreIter = 0; scoreIter < 5; scoreIter++)
		{
			if (name == worldhandler.teamName_t.australia)
			{
				switch (currentTeam.scores[scoreIter])
				{
					case worldhandler.team.kickState_t.FAILED:
						scoreLeft[scoreIter].color = Color.red; break;
					case worldhandler.team.kickState_t.NOT_KICKED:
						scoreLeft[scoreIter].color = Color.black; break;
					case worldhandler.team.kickState_t.SUCCESS:
						scoreLeft[scoreIter].color = Color.green; break;
				}
			}

			else
			{
				switch (currentTeam.scores[scoreIter])
				{
					case worldhandler.team.kickState_t.FAILED:
						scoreRight[scoreIter].color = Color.red; break;
					case worldhandler.team.kickState_t.NOT_KICKED:
						scoreRight[scoreIter].color = Color.black; break;
					case worldhandler.team.kickState_t.SUCCESS:
						scoreRight[scoreIter].color = Color.green; break;
				}
			}
		}
	}

	public void UpdateScores(worldhandler.team team1, worldhandler.team team2)
	{
		team1Score.text = "Australia: " + team1.totalScore;
		team2Score.text = "England: " + team2.totalScore;
	}
}
