﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenu : MonoBehaviour {

	//draw the stadium in the background in a camera rotation
	public Canvas menuCanvas;
	//public Camera menuCamera;
	public float canvasDistance = 10.0f;
	public GameObject handlerObject = null;
	public worldhandler handler = null;

	Button continueButton;
	public static Button[] buttons;

	public MovieTexture movie;
	GameObject intro;
	float introDelay = 0.0f;

	// Use this for initialization
	void Start () {
		//menuCamera = gameObject.AddComponent<Camera> () as Camera;
		if (handlerObject != null)
		{
			handler = handlerObject.GetComponent<worldhandler>() as worldhandler;
		}
		//disable continue button
		continueButton = GameObject.Find("Continue").GetComponent<Button>();
		GameObject.Find("Intro").GetComponent<RawImage>().texture = movie as MovieTexture;
		buttons = GetComponentsInChildren<Button>();
		intro = GameObject.Find("Intro");

		//movie.Play();
		HideContinue(false);

		//hide all buttons
		HideObjects(false);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool PlayIntro()
	{
		if(introDelay <= movie.duration)
		{
			introDelay += Time.deltaTime;
			return true;
		}

		else
		{
			intro.SetActive(false);
			return false;
		}
	}

	public void HideObjects(bool hide)
	{
		//get all button components all disable them
		//gameObject.g
		
		for (uint buttonIndex = 0; buttonIndex < buttons.Length; buttonIndex++)
		{
			buttons[buttonIndex].gameObject.SetActive(hide);
		}
	}

	public void HideContinue(bool hide)
	{
		continueButton.gameObject.SetActive(hide);
	}

	public void HideIntro(bool hide)
	{
		intro.SetActive(hide);
	}

	public void OnNewGame()
	{
		handler.Reset();
		HideObjects(false);
		handler.PlaySound(1, handler.sounds.buttonSound);
		handler.ChangeState(worldhandler.gameStates.BEGIN);
		//pick one of the cameras on random
		//enabled = false; //hid itself
		menuCanvas.enabled = false;
		handler.gamePaused = false;
		//handlerObject.active = false;
		
	}

	public void OnOptions()
	{
		handler.PlaySound(1, handler.sounds.buttonSound);
	}

	public void OnQuit()
	{
		handler.PlaySound(1, handler.sounds.buttonSound);
		Application.Quit ();
	}

	public void OnResume()
	{
		HideObjects(false);
		handler.gamePaused = false;
		handler.ChangeState(worldhandler.gameStates.PLAYER_SETUP);
	}
}
