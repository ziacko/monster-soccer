﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
//using UnityEditor;
using System.Collections;
using System.Globalization;

public class worldhandler : MonoBehaviour
{

	public enum cameraPositions
	{
		//MENU = 0,
		FIRST_PERSON = 0,
		//WIDE_SHOT_LEFT,
		//WIDE_SHOT_RIGHT,
		NET_LEFT,
		NET_RIGHT,
	};

	public enum gameStates
	{
		PLAYER_SETUP = 0,
		PREPARING_KICK,
		DURING_KICK,
		POST_KICK,
		MENU,
		TEAM_WIN,
		BEGIN,
		END,
		STARTUP,
		SUDDEN_DEATH,
	};

	public enum teamName_t
	{
		australia = 0,
		england
	};

	public struct team
	{
		public team(uint numKicks)
		{
			totalScore = 0;
			currentKick = 0;
			scores = new kickState_t[5] { 0, 0, 0, 0, 0};
		}

		public enum kickState_t
		{
			NOT_KICKED = 0,
			FAILED,
			SUCCESS
		};

		public uint totalScore;
		public uint currentKick;
		public kickState_t[] scores;
	}

	public teamName_t currentTeam;
	public team[] teams;

	//use this prefab to speed up instantiation
	public GameObject ballPrefab;
	GameObject gameBall;

	public GameCamera[] gameCameras = new GameCamera[6];
	public uint currentCamera;

	public gameStates currentGameState;

	public MainMenu menu;

	public float teamWinTimer = 3.0f;
	float currentTeamWinTimer = 0.0f;

	public float playerSetupDelay = 3.0f;
	float currentPlayerSetupDelay = 0.0f;
	
	public float duringKickDelay = 1.0f;
	float currentDuringClickDelay = 0.0f;

	public float postKickDelay = 1.5f;
	float currentPostClickDelay = 0.0f;

	public float beginDelay = 1.5f;
	float currentBeginDelsy = 0.0f;

	public float endDelay = 1.5f;
	public float currentEndDelay = 0.0f;

	//public MovieTexture intro;
	float introDelay = 0.0f;

	bool ballKicked = false;
	public team.kickState_t playerAwarded = team.kickState_t.NOT_KICKED;

	public bool gamePaused = false;

	public goalie goalKeeper;
	//uint numTeams = 2;
	public uint winningValue = 5;
	HUD currentHUD;
	public GameObject menuObject;

	public SoundManager sounds;
	AudioSource[] source;

	uint currentRound = 0;
	bool isSuddenDeath = false;

	//there can only be one ball in the world at one time

	// Use this for initialization
	void Start()
	{
		currentTeam = teamName_t.australia;
		teams = new team[2]{new team(5), new team(5)};

		currentGameState = gameStates.STARTUP;
		currentHUD = GameObject.Find("HUD").GetComponent<HUD>();

		source = GetComponents<AudioSource>();
		sounds = GetComponent<SoundManager>();

		menu.movie.Play();
	}

	void HideMenu(bool hide)
	{
		//menuObject.SetActive(hide);
		//gameCameras[(uint)cameraPositions.MENU].gameObject.SetActive(hide);
	}

	void PlayMusic()
	{
		source[0].clip = sounds.defaultMusic;
		source[0].Play();
	}

	public void PlaySound(uint sourceIndex, AudioClip sound, float soundSkip = 0.0f, float soundOffset = 0.0f)
	{
		source[sourceIndex].clip = sound;
		source[sourceIndex].time = soundSkip;
		source[sourceIndex].Play((ulong)soundOffset * 44100);
	}

	bool CheckRound()
	{
		//here is where we start doing round calculations.
		//if the last team was england then add to round number
		//also check for shutout
		//check whether sudden death should begin
		if (isSuddenDeath)
		{
			//if the scores are the same continue
			//else check who is higher and reward the winner
			if(teams[0].totalScore == teams[1].totalScore)
			{
				return false;
			}

			else if(teams[0].totalScore > teams[1].totalScore)
			{
				//the current team has won woo!
				currentHUD.ShowWinner(teamName_t.australia, true);
				ChangeState(gameStates.TEAM_WIN);
				return true;
			}

			else
			{
				//the current team has won woo!
				currentHUD.ShowWinner(teamName_t.england, true);
				ChangeState(gameStates.TEAM_WIN);
				return true;
			}
		}

		else
		{
			switch (currentRound)
			{
				case 3:
					{
						//check if shutout v1
						//if only ONE team (not both)
						if (teams[0].totalScore == 3 && teams[1].totalScore == 3)
						{
							//if BOTH are at 3 then continue on
							return false;
						}

						//if one of them scores zero and the other is 3 then it is a shutout
						else if ((teams[0].totalScore == 3 || teams[0].totalScore == 0) && (teams[1].totalScore == 3 || teams[1].totalScore == 0))
						{
							//whichever team has the higher score in this case has shut out the other team
							if (teams[0].totalScore > teams[1].totalScore)
							{
								currentHUD.ShowWinner(teamName_t.australia, true);
								ChangeState(worldhandler.gameStates.TEAM_WIN);
								//team 2 has been shutout
								return true;
							}

							else if (teams[1].totalScore > teams[0].totalScore)
							{
								currentHUD.ShowWinner(teamName_t.england, true);
								ChangeState(worldhandler.gameStates.TEAM_WIN);
								//team 1 has been shutout
								return true;
							}

							else
							{
								return false;
							}
						}

						else
						{
							//else just continue on
							return false;
						}
					}

				case 4:
					{
						//check if shutout v2
						//this will be much harder to determine a shutout

						//misses = (roundNumber - teamscore)
						//max possible score(MPS) = 5 - misses
						uint team1Misses = currentRound - teams[0].totalScore;
						uint team1MPS = 5 - team1Misses;

						uint team2Misses = currentRound - teams[1].totalScore;
						uint team2MPS = 5 - team2Misses;

						//if mps and the opposite team score are equal then the team still has a chance
						if (team1MPS == teams[1].totalScore || team2MPS == teams[0].totalScore)
						{
							return false;
						}

						else if (teams[0].totalScore > team2MPS)
						{
							currentHUD.ShowWinner(teamName_t.australia, true);
							ChangeState(worldhandler.gameStates.TEAM_WIN);
							//team 1 has won since team 2 cannot possibly win
							return true;
						}

						else if (teams[1].totalScore > team1MPS)
						{
							currentHUD.ShowWinner(teamName_t.england, true);
							ChangeState(worldhandler.gameStates.TEAM_WIN);
							return true;
							//team 2 has won since team 1 cannot possibly win
						}

						else
						{
							return false;
						}

					}

				case 5:
					{
						//who won?
						//do we need to go into sudden death mode?
						if (teams[0].totalScore == teams[1].totalScore)
						{
							//ChangeState(gameStates.SUDDEN_DEATH);
							isSuddenDeath = true;
							currentHUD.HideScores(false); //just disable the scores
							currentHUD.ShowSuddenDeath("Sudden Death!");
							return false;
							//sudden death time!

						}
						else if (teams[0].totalScore > teams[1].totalScore)
						{
							currentHUD.ShowWinner(teamName_t.australia, true);
							ChangeState(worldhandler.gameStates.TEAM_WIN);
							//team 1 has won!
						}

						else
						{
							currentHUD.ShowWinner(teamName_t.england, true);
							ChangeState(worldhandler.gameStates.TEAM_WIN);
							//team 2 has won!
						}
						return true;
					}

				default:
					{
						return false;
					}
			}
		}
	}

	public void ChangeState(gameStates newState)
	{
		currentGameState = newState;
		currentTeamWinTimer = 0.0f;
		currentPlayerSetupDelay = 0.0f;
		currentPostClickDelay = 0.0f;
		currentDuringClickDelay = 0.0f;
		currentEndDelay = 0.0f;
		currentBeginDelsy = 0.0f;
		Time.timeScale = 1.0f;

		switch (newState)
		{
			case gameStates.MENU:
				{
					PlayMusic();
					currentHUD.Disable(true);
					int cameraSwitch = Random.Range((int)worldhandler.cameraPositions.FIRST_PERSON, gameCameras.Length);
					SwitchCamera(cameraSwitch);
					currentHUD.DisableHUD(false);
					break;
				}

			case gameStates.PLAYER_SETUP:
				{
					//play the crowd sounds
					PlaySound(1, sounds.crowdRandom[Random.Range(0, sounds.crowdRandom.Length)]);
					//goalKeeper.Reset();
					if(playerAwarded == team.kickState_t.FAILED)
					{
						goalKeeper.Roar();
					}

					else
					{
						goalKeeper.Reset();
					}
					
					Destroy(gameBall);
					ballKicked = false;
					if(!isSuddenDeath)
					{
						currentHUD.DisableHUD(true);
					}
					
					currentHUD.UpdateScores(teams[0], teams[1]);
					//currentHUD.flavorText.enabled = false;
					currentHUD.goalImage.enabled = false;
					//show splash screen and flavor text here

					int cameraSwitch = Random.Range((int)worldhandler.cameraPositions.NET_LEFT, gameCameras.Length);
					//Debug.Log(cameraSwitch.ToString());
					SwitchCamera(cameraSwitch);

					break;
				}

			case gameStates.DURING_KICK:
				{
					break;
				}

			case gameStates.PREPARING_KICK:
				{
					//if the last team was england then up round count
					if (currentTeam == teamName_t.australia)
					{
						currentHUD.team1Image.color = Color.blue;
						currentHUD.team2Image.color = new Color(0.0f, 0.0f, 0.0f, 0.25f);
					}

					else
					{
						currentHUD.team2Image.color = Color.blue;
						currentHUD.team1Image.color = new Color(0.0f, 0.0f, 0.0f, 0.25f);
					}

					PlaySound(0, sounds.stadiumAmbient);
					PlaySound(1, sounds.whistleSound);
					playerAwarded = team.kickState_t.NOT_KICKED;
					if (!isSuddenDeath)
					{
						currentHUD.DisableHUD(true);
					}
					currentHUD.UpdateScores(teams[0], teams[1]);
					//currentHUD.flavorText.enabled = false;
					//currentHUD.splashImage.enabled = false;
					SwitchCamera((int)cameraPositions.FIRST_PERSON);
					break;
				}

			case gameStates.POST_KICK:
				{
					if (currentTeam == teamName_t.england)
					{
						currentRound++;
						if (CheckRound())
						{
							return;
						}
					}
					SwitchTeam();
					break;
				}

			case gameStates.TEAM_WIN:
				{
					int cameraSwitch = Random.Range((int)worldhandler.cameraPositions.NET_LEFT, gameCameras.Length);
					break;
				}

			case gameStates.BEGIN:
				{
					//currentHUD.splashImage.enabled = true;
					
					break;
				}

			case gameStates.END:
				{
					//currentHUD.splashImage.enabled = true;
					break;
				}

			case gameStates.SUDDEN_DEATH:
				{
					break;
				}
		}
	}

	public void SwitchCamera(int cameraIndex)
	{
		currentCamera = (uint)cameraIndex;
		if (gameCameras[currentCamera].gameCamera != null)
		{
			gameCameras[currentCamera].gameCamera.enabled = true;
			gameCameras[currentCamera].GetComponent<AudioListener>().enabled = true;
			Camera.SetupCurrent(gameCameras[currentCamera].gameCamera);

			//for each camera that is not the current camera disable it
			for (uint cameraIter = 0; cameraIter < gameCameras.Length; cameraIter++)
			{
				if (gameCameras[cameraIter] != null && cameraIter != currentCamera)
				{
					gameCameras[cameraIter].Reset();
					gameCameras[cameraIter].gameCamera.enabled = false;
					gameCameras[cameraIter].GetComponent<AudioListener>().enabled = false;
				}
			}
		}
	}

	void SwitchTeam()
	{
		//use a quick ternary operator
		currentTeam = (currentTeam == teamName_t.australia) ? teamName_t.england : teamName_t.australia;
	}

	void KickBall()
	{
		if (!ballKicked)
		{
			RaycastHit vHit = new RaycastHit();
			Ray mouseRay = gameCameras[currentCamera].gameCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 2));
			if (Physics.Raycast(mouseRay, out vHit))
			{

			}

			Debug.DrawRay(mouseRay.origin, mouseRay.direction, Color.red, float.PositiveInfinity);
			Transform point = transform;
			point.LookAt(mouseRay.direction);
			gameBall = Instantiate(ballPrefab, mouseRay.origin, point.rotation) as GameObject;
			gameBall.transform.LookAt(mouseRay.direction);
			gameBall.GetComponent<Rigidbody>().AddForce(mouseRay.direction * Ball.ballSpeed);
			ballKicked = true;
			goalKeeper.Leap();
			ChangeState(gameStates.DURING_KICK);
		}
	}

	public void AwardPlayer()
	{
		if (playerAwarded == team.kickState_t.NOT_KICKED)
		{
			PlaySound(0, sounds.goalSuccess);

			playerAwarded = team.kickState_t.SUCCESS;
			if (!isSuddenDeath)
			{
				teams[(int)currentTeam].scores[teams[(int)currentTeam].currentKick] = team.kickState_t.SUCCESS;
				teams[(int)currentTeam].currentKick++;
			}

			teams[(int)currentTeam].totalScore++;

			currentHUD.UpdateBoard(teams[(int)currentTeam], currentTeam);
			currentHUD.UpdateScores(teams[0], teams[1]);
		}
	}

	public void DisqualifyPlayer()
	{
		if (playerAwarded == team.kickState_t.NOT_KICKED)
		{
			playerAwarded = team.kickState_t.FAILED;
			if (!isSuddenDeath)
			{
				teams[(int)currentTeam].scores[teams[(int)currentTeam].currentKick] = team.kickState_t.FAILED;
				teams[(int)currentTeam].currentKick++;
			}

			currentHUD.UpdateBoard(teams[(int)currentTeam], currentTeam);
			//SwitchTeam();
		}
	}

	public void Reset()
	{
		//menu.HideObjects(true);
		goalKeeper.Reset();
		isSuddenDeath = false;
		currentRound = 0;
		//Start();
		currentHUD.reset();
		menu.HideObjects(false);
		playerAwarded = team.kickState_t.NOT_KICKED;
		for(uint iter = 0; iter < teams.Length; iter++)
		{
			teams[iter].totalScore = 0;
			teams[iter].currentKick = 0;

			for (uint scoreIter = 0; scoreIter < 5; scoreIter++)
			{
				teams[iter].scores[scoreIter] = team.kickState_t.NOT_KICKED;
			}
		}
		currentTeam = teamName_t.australia;
		ChangeState(gameStates.MENU);
	}

	// Update is called once per frame
	void Update()
	{
		//if at any point escape key is pressed then pause
		if (!gamePaused)
		{
			if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space))
			{
				ChangeState(gameStates.MENU);
				gamePaused = true;
				//menu.enabled = false;//			GameObject.Find("Continue").SetActive(false);
				menu.HideContinue(true);
				menu.HideObjects(true);
			}
		}

		if (!gamePaused)
		{
			switch (currentGameState)
			{
				//at the beginning of the game or if the player ever presses escape
				case gameStates.MENU:
					{
						break;
					}

				//while the ball is moving through the air. if the ball is in the goal then swap to net cameras
				case gameStates.DURING_KICK:
					{
						//if the player has kicked the ball, just let it fly until it hits something
						//if the ball hasn't done shit then skip ahead
						if (currentDuringClickDelay < duringKickDelay)
						{
							currentDuringClickDelay += Time.deltaTime;

							if (playerAwarded == team.kickState_t.SUCCESS)
							{
								gameCameras[currentCamera].TrackBall(gameBall);
							}
						}

						else
						{
							DisqualifyPlayer();
							ChangeState(worldhandler.gameStates.POST_KICK);
						}

						break;
					}

				//back to wide shot and display totalScore.
				case gameStates.POST_KICK:
					{
						//after the ball has either hit or missed the goal.
						//move camera back to the wideshot. randomize?

						if (currentPostClickDelay < postKickDelay)
						{
							currentPostClickDelay += Time.deltaTime;
							if (goalKeeper.caughtBall)
							{
								if (ballPrefab != null)
								{
									Vector3 midpoint = Vector3.Lerp(goalKeeper.leftHand.transform.position, goalKeeper.rightHand.transform.position, 0.5f);
									gameBall.transform.position = midpoint;
								}
							}
						}

						else
						{
							ChangeState(gameStates.PLAYER_SETUP);
						}

						break;
					}

				//this is when the player is gearing up for the kick
				case gameStates.PREPARING_KICK:
					{
						if (Input.GetMouseButtonDown(0))
						{
							//if the ball hasn't already been kicked then fire away
							KickBall();
						}
						break;
					}

				//after post kick, we show a wide shot of the goal
				//this is to make sure players have enough time to get into position
				case gameStates.PLAYER_SETUP:
					{

						//just run down the timer then move to prepare kick
						if (currentPlayerSetupDelay < playerSetupDelay)
						{
							currentPlayerSetupDelay += Time.deltaTime;
						}
						else
						{
							ChangeState(gameStates.PREPARING_KICK);
						}
						break;
					}

				case gameStates.TEAM_WIN:
					{
						
						if (currentTeamWinTimer < teamWinTimer)
						{
							currentTeamWinTimer += Time.deltaTime;
						}

						else
						{
							ChangeState(gameStates.END);
						}
						break;
					}

				case gameStates.BEGIN:
					{
						if(currentBeginDelsy < beginDelay)
						{
							currentBeginDelsy += Time.deltaTime;
						}

						else
						{
							ChangeState(gameStates.PLAYER_SETUP);
						}
						break;
					}

				case gameStates.END:
					{
						if(currentEndDelay < endDelay)
						{
							currentEndDelay += Time.deltaTime;
						}

						else
						{
							Reset();
							menu.HideObjects(true);
						}
						break;
					}

				case gameStates.STARTUP:
					{
						//when startup ends
						if(introDelay < menu.movie.duration)
						{
							introDelay += Time.deltaTime;
						}

						else
						{
							menu.HideIntro(false);
							menu.HideObjects(true);
							PlayMusic();
							ChangeState(gameStates.MENU);
						}
						
						break;
					}
			}
		}
	}

	public void UnPause()
	{
		gamePaused = true;
		//get the continue button in the menu and hide it
		//then hide the rest of the menu
		
		ChangeState(gameStates.PLAYER_SETUP);
	}
}
