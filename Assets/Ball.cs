﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ball : MonoBehaviour {

	enum objectHit
	{
		GOALIE = 0,
		GOAL_POST,
		NET,
		MISSED
	}

	public GameObject handlerObject = null;
	public worldhandler handler = null;
	public Rigidbody rigid;
	public SphereCollider sphere;
	public const float ballSpeed = 1000.0f;
	const float defaultBallLifeSpan = 2.0f;
	float currentBallLifeSpan = 0.0f;
	Renderer render;
	Material material;

	AudioSource audio;

	// Use this for initialization
	void Start () {
		//body = GetComponent<Rigidbody>();
		//body.AddForce(transform.forward * ballSpeed);
		Debug.DrawRay(transform.position, transform.forward, Color.blue, float.PositiveInfinity);
		//handlerObject =
		handler = GameObject.FindObjectOfType<worldhandler>() as worldhandler;
		rigid = GetComponent<Rigidbody>();
		sphere = GetComponent<SphereCollider>();
		audio = GetComponent<AudioSource>();
		audio.playOnAwake = false;
		render = GetComponent<Renderer>();
		material = render.sharedMaterial;

		material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;


	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter(Collision col)
	{
		//Debug.Log(col.gameObject.name);
		GetComponent<TrailRenderer>().enabled = false;
		//rigid.mass = 5.0f;
		//rigid.velocity = -transform.forward;
		if (col.gameObject.name == "Goalie")
		{
			if (handler.playerAwarded == worldhandler.team.kickState_t.NOT_KICKED)
			{
				//goalie has caught it
				//sphere.enabled = false;
				//rigid.useGravity = false;
				//rigid.isKinematic = true;
				//sphere.enabled = false;
				//rigid.velocity = (-rigid.velocity * 0.25f);
				//rigid.mass = 0.0f;
				handler.goalKeeper.caughtBall = false;
				//handler.ChangeState(worldhandler.gameStates.POST_KICK);
				handler.PlaySound(0, handler.sounds.goalFail, 0.5f);
				handler.DisqualifyPlayer();
				render.material.SetColor("_EmissionColor", Color.red);

				//if the ball hist the goalie, look back at the handler's camera
				//and have the ball fly back towards it
				transform.LookAt(handler.gameCameras[handler.currentCamera].transform);
				Ray cameraRay = new Ray(transform.position, transform.forward);
				//Debug.DrawRay(transform.position, transform.forward);

				rigid.AddForce(cameraRay.direction * ballSpeed);

				rigid.velocity = transform.forward;
				Time.timeScale = 0.25f;
			}
		}

		else // just missed everything like a chump
		{
			handler.goalKeeper.caughtBall = false;

			//rigid.useGravity = true;
	
				if (col.gameObject.name == "goal 1")
				{
					audio.Play();
				}	
		}
	}

	void OnTriggerEnter(Collider col)
	{
		//if the ball has passed the goalie and entered the net 
		//then find the closer side camera and render from there
		//transform.position
		//Debug.Log(col.gameObject.name);
		//get the distance between the two cameras and see which is closer
		if (handler.currentGameState == worldhandler.gameStates.DURING_KICK)
		{

			if (col.gameObject.name == "Left Hand" || col.gameObject.name == "Right Hand")
			{
				rigid.useGravity = false;
				rigid.isKinematic = true;
				sphere.enabled = false;
				handler.goalKeeper.caughtBall = true;
				//handler.ChangeState(worldhandler.gameStates.POST_KICK);
				handler.PlaySound(0, handler.sounds.goalFail, 0.5f);
				handler.DisqualifyPlayer();
				render.material.SetColor("_EmissionColor", Color.red);
			}

			else
			{
				Time.timeScale = 0.5f;
				//float leftDistance = Vector3.Distance(handler.gameCameras[(uint)worldhandler.cameraPositions.NET_LEFT].transform.position, transform.position);
				//float rightDistance = Vector3.Distance(handler.gameCameras[(uint)worldhandler.cameraPositions.NET_RIGHT].transform.position, transform.position);

				handler.AwardPlayer();
				int newCamera = Random.Range((int)worldhandler.cameraPositions.NET_LEFT, handler.gameCameras.Length);
				handler.SwitchCamera(newCamera);
				//DynamicGI.SetEmissive(render, Color.green);
				//DynamicGI.UpdateMaterials(render);
				//DynamicGI.UpdateEnvironment();
				//render.material.SetColor("_EmissionColor", Color.green);
			}
			/*if (Mathf.Min(leftDistance, rightDistance) == leftDistance)
			{
				handler.SwitchCamera(worldhandler.cameraPositions.NET_LEFT);
			}

			else
			{
				handler.SwitchCamera(worldhandler.cameraPositions.NET_RIGHT);
			}*/
		}
	}
}
